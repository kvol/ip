﻿using System;
using AI.Utils;
using UnityEngine;

namespace AI
{
    [RequireComponent(typeof(Transform)), ExecuteInEditMode]
    public class Agent : MonoBehaviour
    {
        [SerializeField] private float _maxSpeed;
        [SerializeField] private float _maxAcceleration;
        [SerializeField] private float _maxRotation;
        [SerializeField] private float _maxAngularAcceleration;
        [SerializeField] private float _orientation;
        private float _rotation;
        private Vector3 _velocity;
        protected Steering Steering;

        public float MAXSpeed => _maxSpeed;

        public float MAXRotation => _maxRotation;

        public float MAXAngularAcceleration => _maxAngularAcceleration;

        public float Orientation
        {
            get => _orientation;
            set => _orientation = value;
        }

        public float Rotation => _rotation;

        public Vector3 Velocity => _velocity;

        public float MAXAcceleration => _maxAcceleration;

        private Transform _transform;

        private void Start()
        {
            _transform = transform;
            _velocity = Vector3.zero;
            Steering = new Steering();
            Debug.Log(3 << 2);
            var a = new int[2, 3, 4, 5];
        }

        public void SetSteering(Steering steering)
        {
            Steering = steering;
        }

        private void Update()
        {
            Vector3 displacement = _velocity * Time.deltaTime;
            _orientation += _rotation * Time.deltaTime;

            if (_orientation < 0.0f)
            {
                _orientation += 360.0f;
            }
            else if (_orientation > 360.0f)
            {
                _orientation -= 360.0f;
            }

            _transform.Translate(displacement, Space.World);
            _transform.rotation = new Quaternion();
            _transform.Rotate(Vector3.up, _orientation);
        }

        private void LateUpdate()
        {
            _velocity += Steering.Linear * Time.deltaTime;
            _rotation += Steering.Angular * Time.deltaTime;

            _velocity = Vector3.ClampMagnitude(_velocity, _maxSpeed);
            if (Steering.Angular == 0)
            {
                _rotation = 0;
            }

            if (Steering.Linear.sqrMagnitude == 0)
            {
                _velocity = Vector3.zero;
            }

            Steering = new Steering();
        }
    }
}