﻿using UnityEngine;

namespace AI.Utils
{
    public class PathSegment
    {
        public Vector3 StartPoint;
        public Vector3 EndPoint;

        public PathSegment() : this(Vector3.zero, Vector3.zero)
        {
        }

        public PathSegment(Vector3 startPoint, Vector3 endPoint)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
        }
    }
}