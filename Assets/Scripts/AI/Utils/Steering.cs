﻿using UnityEngine;

namespace AI.Utils
{
    public class Steering
    {
        public float Angular { get; set; }

        public Vector3 Linear { get; set; }


        public Steering()
        {
            Angular = 0;
            Linear = new Vector3();
        }
    }
}