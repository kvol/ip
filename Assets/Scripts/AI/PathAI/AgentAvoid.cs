﻿using System;
using AI.AgentBehaviours;
using AI.Utils;
using UnityEngine;

namespace AI.PathAI
{
    public class AgentAvoid : AgentBehaviour
    {
        [SerializeField] private float _collisionRadius = 0.4f;
        private GameObject[] _targets;

        private void Start()
        {
            _targets = GameObject.FindGameObjectsWithTag("Agent");
        }

        protected override Steering GetSteering()
        {
            Steering steering = new Steering();
            float shortestTime = Mathf.Infinity;
            GameObject firstTarget = null;
            float firstMinSeparation = 0;
            float firstDistance = 0;
            Vector3 firstRelativePosition = Vector3.zero;
            Vector3 firstRelativeVelocity = Vector3.zero;

            foreach (var target in _targets)
            {
                Vector3 relativePosition;
                Agent targetAgent = target.GetComponent<Agent>();
                relativePosition = target.transform.position - transform.position;
                Vector3 relativeVelocity = targetAgent.Velocity - Agent.Velocity;
                float relativeSpeed = relativeVelocity.magnitude;
                float timeToCollision = Vector3.Dot(relativePosition, relativeVelocity);
                timeToCollision /= relativeSpeed * relativeSpeed * -1;
                float distance = relativePosition.magnitude;
                float minSeparation = distance - relativeSpeed * timeToCollision;
                if (minSeparation > 2 * _collisionRadius)
                {
                    continue;
                }

                if (timeToCollision > 0 && timeToCollision < shortestTime)
                {
                    shortestTime = timeToCollision;
                    firstTarget = target;
                    firstMinSeparation = minSeparation;
                    firstRelativePosition = relativePosition;
                    firstRelativeVelocity = relativeVelocity;
                }
            }

            if (firstTarget == null)
                return steering;
            if (firstMinSeparation <= 0 || firstDistance < 2 * _collisionRadius)
            {
                firstRelativePosition = firstTarget.transform.position;
            }
            else
            {
                firstRelativePosition += firstRelativeVelocity * shortestTime;
            }

            firstRelativePosition.Normalize();
            steering.Linear = -firstRelativePosition * Agent.MAXAcceleration;
            return steering;
        }
    }
}