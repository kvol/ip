﻿using AI.AgentBehaviours.BasicMovement.Seek;
using AI.Utils;
using UnityEngine;

namespace AI.PathAI
{
    public class PathFollower : Seek
    {
        [SerializeField] private Path _path;
        [SerializeField] private float _pathOffset = 0;
        private float _currentParameter;

        public override void Awake()
        {
            base.Awake();
            Target = new GameObject();
            _currentParameter = 0;
        }

        protected override Steering GetSteering()
        {
            _currentParameter = _path.GetParam(transform.position, _currentParameter);
            float targetParameter = _currentParameter + _pathOffset;
            Target.transform.position = _path.GetPosition(targetParameter);
            return base.GetSteering();
        }
    }
}