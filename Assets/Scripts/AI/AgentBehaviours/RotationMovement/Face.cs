﻿using AI.Utils;
using UnityEngine;

namespace AI.AgentBehaviours.RotationMovement
{
    public class Face : Align
    {
        protected GameObject TargetAux;

        public override void Awake()
        {
            base.Awake();
            TargetAux = Target;
            Target = new GameObject();
            Target.AddComponent<Agent>();
        }

        private void OnDestroy()
        {
            Destroy(Target);
        }

        protected override Steering GetSteering()
        {
            Vector3 direction = TargetAux.transform.position - transform.position;
            if (direction.magnitude > 0)
            {
                float targetOrientation = Mathf.Atan2(direction.x, direction.z);
                targetOrientation *= Mathf.Rad2Deg;
                Target.GetComponent<Agent>().Orientation = targetOrientation;
            }

            return base.GetSteering();
        }
    }
}