﻿using AI.Utils;
using UnityEngine;

namespace AI.AgentBehaviours.RotationMovement
{
    public class Align : AgentBehaviour
    {
        [SerializeField] private float _targetRadius;
        [SerializeField] private float _slowRadius;
        [SerializeField] private float _timeToTarget = 0.1f;

        protected override Steering GetSteering()
        {
            // Steering steering = new Steering();
            // float targetOrientation = Target.GetComponent<Agent>().Orientation;
            //
            // float rotation = MapToRange(targetOrientation - Agent.Orientation);
            // float rotationSize = Mathf.Abs(rotation);
            // if (rotationSize < _targetRadius)
            // {
            //     return steering;
            // }
            //
            // float targetRotation;
            // if (rotationSize > _slowRadius)
            // {
            //     targetRotation = Agent.MAXRotation;
            // }
            // else
            // {
            //     targetRotation = Agent.MAXRotation * rotationSize / _slowRadius;
            // }
            //
            // targetRotation *= rotation / rotationSize;
            // steering.Angular = (targetRotation - Agent.Rotation) / _timeToTarget;
            // float angularAccel = Mathf.Abs(steering.Angular);
            // if (angularAccel > Agent.MAXAngularAcceleration)
            // {
            //     steering.Angular /= angularAccel;
            //     steering.Angular *= Agent.MAXAngularAcceleration;
            // }
            //
            // return steering;
            Steering steering = new Steering();
            float targetOrientation = Target.GetComponent<Agent>().Orientation;
            float rotation = targetOrientation - Agent.Orientation;
            rotation = MapToRange(rotation);
            float rotationSize = Mathf.Abs(rotation);
            if (rotationSize < _targetRadius)
                return steering;
            float targetRotation;
            if (rotationSize > _slowRadius)
                targetRotation = Agent.MAXRotation;
            else
                targetRotation = Agent.MAXRotation * rotationSize / _slowRadius;
            targetRotation *= rotation / rotationSize;
            steering.Angular = targetRotation - Agent.Rotation;
            steering.Angular /= _timeToTarget;
            float angularAccel = Mathf.Abs(steering.Angular);
            if (angularAccel > Agent.MAXAngularAcceleration)
            {
                steering.Angular /= angularAccel;
                steering.Angular *= Agent.MAXAngularAcceleration;
            }

            return steering;
        }
    }
}