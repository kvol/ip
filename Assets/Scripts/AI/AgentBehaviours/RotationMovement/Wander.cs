﻿using AI.Utils;
using UnityEngine;

namespace AI.AgentBehaviours.RotationMovement
{
    public class Wander : Face
    {
        [SerializeField] private float _offset;
        [SerializeField] private float _radius;
        [SerializeField] private float _rate;

        public override void Awake()
        {
            Target = new GameObject();
            Target.transform.position = transform.position;
            base.Awake();
        }

        protected override Steering GetSteering()
        {
            // Steering steering = new Steering();
            // float wanderOrientation = Random.Range(-1.0f, 1.0f) * _rate;
            // float targetOrientation = wanderOrientation + Agent.Orientation;
            // Vector3 orientationVec = OrientationToVector(Agent.Orientation);
            // Vector3 targetPosition = (_offset * orientationVec) + transform.position;
            // targetPosition += OrientationToVector(targetOrientation) * _radius;
            // TargetAux.transform.position = targetPosition;
            // steering = base.GetSteering();
            // steering.Linear = (TargetAux.transform.position - transform.position).normalized * Agent.MAXAcceleration;
            // return steering;
            Steering steering = new Steering();
            float wanderOrientation = Random.Range(-1.0f, 1.0f) * _rate;
            float targetOrientation = wanderOrientation + Agent.Orientation;
            Vector3 orientationVec = OrientationToVector(Agent.Orientation);
            Vector3 targetPosition = (_offset * orientationVec) + transform.position;
            targetPosition = targetPosition + (OrientationToVector(targetOrientation) * _radius);
            TargetAux.transform.position = targetPosition;
            steering = base.GetSteering();
            steering.Linear = TargetAux.transform.position - transform.position;
            steering.Linear.Normalize();
            steering.Linear *= Agent.MAXAcceleration;
            return steering;
        }
    }
}