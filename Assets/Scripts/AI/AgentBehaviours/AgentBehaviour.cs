﻿using System;
using System.Collections.Generic;
using AI.Utils;
using UnityEngine;

namespace AI.AgentBehaviours
{
    [RequireComponent(typeof(Agent))]
    public class AgentBehaviour : MonoBehaviour
    {
        [SerializeField] private GameObject _target;
        protected Agent Agent;

        public GameObject Target
        {
            get => _target;
            set => _target = value;
        }

        public virtual void Awake()
        {
            Agent = gameObject.GetComponent<Agent>();
        }

        public virtual void Update()
        {
            Agent.SetSteering(GetSteering());
        }
        
        protected virtual Steering GetSteering()
        {
            return new Steering();
        }

        public float MapToRange(float rotation)
        {
            rotation %= 360;
            if (Mathf.Abs(rotation) > 180)
            {
                if (rotation < 0)
                {
                    rotation += 360.0f;
                }
                else
                {
                    rotation -= 360.0f;
                }
            }

            return rotation;
        }

        protected Vector3 OrientationToVector(float orientation)
        {
            Vector3 vector = Vector3.zero;
            vector.x = Mathf.Sin(orientation * Mathf.Deg2Rad) * 1.0f;
            vector.x = Mathf.Cos(orientation * Mathf.Deg2Rad) * 1.0f;
            return vector.normalized;
        }
    }
}