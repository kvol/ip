﻿using AI.Utils;
using UnityEngine;

namespace AI.AgentBehaviours.BasicMovement.Seek
{
    public class Arrive : AgentBehaviour
    {
        [SerializeField] private float _targetRadius;
        [SerializeField] private float _slowRadius;
        [SerializeField] private float _timeToTarget = 0.1f;

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, _slowRadius);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _targetRadius);
        }

        protected override Steering GetSteering()
        {
            Steering steering = new Steering();
            Vector3 direction = Target.transform.position - transform.position;

            float distance = direction.magnitude;

            float targetSpeed;

            if (distance < _targetRadius)
            {
                return steering;
            }

            if (distance > _slowRadius)
            {
                targetSpeed = Agent.MAXSpeed;
            }
            else
            {
                targetSpeed = Agent.MAXSpeed * distance / _slowRadius;
            }

            Vector3 desiredVelocity = direction.normalized * targetSpeed;
            steering.Linear = (desiredVelocity - Agent.Velocity) / _timeToTarget;

            if (steering.Linear.magnitude > Agent.MAXAcceleration)
            {
                steering.Linear = steering.Linear.normalized * Agent.MAXAcceleration;
            }

            return steering;
        }
    }
}