﻿using AI.Utils;

namespace AI.AgentBehaviours.BasicMovement.Seek
{
    public class Seek : AgentBehaviour
    {
        protected override Steering GetSteering()
        {
            Steering steering = new Steering
            {
                Linear = (Target.transform.position - transform.position).normalized * Agent.MAXAcceleration
            };
            return steering;
        }
    }
}