﻿using AI.Utils;

namespace AI.AgentBehaviours.Flee
{
    public class Flee : AgentBehaviour
    {
        protected override Steering GetSteering()
        {
            Steering steering = new Steering
            {
                Linear = (transform.position - Target.transform.position).normalized * Agent.MAXAcceleration
            };
            return steering;
        }
    }
}