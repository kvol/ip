﻿using AI.Utils;
using UnityEngine;

namespace AI.AgentBehaviours.BasicMovement.Flee
{
    public class Leave : AgentBehaviour
    {
        [SerializeField] private float _escapeRadius;
        [SerializeField] private float _dangerRadius;
        [SerializeField] private float _timeToTarget = 0.1f;

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, _dangerRadius);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _escapeRadius);
        }

        protected override Steering GetSteering()
        {
            Steering steering = new Steering();
            Vector3 direction = transform.position - Target.transform.position;

            float distance = direction.magnitude;

            float reduce;

            if (distance > _dangerRadius)
            {
                return steering;
            }

            if (distance < _escapeRadius)
            {
                reduce = 0;
            }
            else
            {
                reduce = Agent.MAXSpeed * distance / _dangerRadius;
            }

            float targetSpeed = Agent.MAXSpeed - reduce;

            Vector3 desiredVelocity = direction.normalized * targetSpeed;
            steering.Linear = (desiredVelocity - Agent.Velocity) / _timeToTarget;

            if (steering.Linear.magnitude > Agent.MAXAcceleration)
            {
                steering.Linear = steering.Linear.normalized * Agent.MAXAcceleration;
            }

            return steering;
        }
    }
}