﻿using AI;
using AI.AgentBehaviours;
using AI.AgentBehaviours.BasicMovement.Flee;
using AI.AgentBehaviours.BasicMovement.Seek;
using AI.AgentBehaviours.Flee;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class RoleChanger : MonoBehaviour
{
    [SerializeField] private Agent _agent1;
    private bool _isAgent1Purse;
    [SerializeField] private Agent _agent2;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Agent agent))
        {
            if (agent.Equals(_agent1) && !_isAgent1Purse)
            {
                _agent1.GetComponent<Seek>().enabled = true;
                _agent1.GetComponent<Flee>().enabled = false;

                _agent2.GetComponent<Seek>().enabled = false;
                _agent2.GetComponent<Flee>().enabled = true;
                _isAgent1Purse = !_isAgent1Purse;
            }

            if (agent.Equals(_agent2) && _isAgent1Purse)
            {
                _agent2.GetComponent<Seek>().enabled = true;
                _agent2.GetComponent<Flee>().enabled = false;

                _agent1.GetComponent<Seek>().enabled = false;
                _agent1.GetComponent<Flee>().enabled = true;
                _isAgent1Purse = !_isAgent1Purse;
            }
        }
    }
}